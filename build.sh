#!/usr/bin/env bash

# Build image
docker-compose up

# All useful data mapped from the container
mkdir -p jenkins_home/ logs/nginx/ certs/
