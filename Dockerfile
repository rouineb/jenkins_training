FROM jenkins/jenkins:lts
LABEL author ROUINEB Hamza <rouineb.work@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive
ARG docker_version=17.12.0~ce-0~debian

USER root
RUN apt-get update && apt-get -y install \
    apt-transport-https \
    ca-certificates \
    apt-utils \
    curl \
    gnupg2 \
    software-properties-common \
  && curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > \
    /tmp/dkey; apt-key add /tmp/dkey \
  && add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
    $(lsb_release -cs) \
    stable" \
  && apt-get update && apt-get -y install \
    docker-ce=${docker_version}

ENTRYPOINT [ "/usr/local/bin/jenkins.sh" ]