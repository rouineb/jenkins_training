# Jenkins LTS - Nginx - Docker

Le but de cette image est de faciliter la mise en place de Jenkins + un reverse proxy configurable selon les variables d'environnements passées.

Pour builder les images Jenkins & Nginx, il suffit juste d'executer la commande suivante :

```console
$ ./build.sh
```

Et après (-d est optionnel, pour lancer les conteneurs en mode détaché)

```console
$ docker-compose up [-d]
```

Une fois lancé, pour récupérer le mot de passe il suffit de lancer la commande suivante :

```console
$ docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

### Nginx configuration

Puisque `default.conf` est chargé directement depuis la machine hôte, il est désormais possible de modifier la confguration en temps réel. Une fois
les modifications réalisées, l'utilisateur doit recharger le serveur :

- Exemple de validation:

  ```console
  $ docker exec nginx /usr/sbin/nginx -t
  nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
  nginx: configuration file /etc/nginx/nginx.conf test is successful
  ```

- Exemple de rechargement:

  ```console
  $ docker exec nginx /usr/sbin/nginx -s reload
  Reloading nginx: nginx.
  ```

## References

[1] Official Jenkins Docker Image: [https://github.com/jenkinsci/docker](https://github.com/jenkinsci/docker)

[2] Get into DevOps (blog): [https://getintodevops.com/blog/the-simple-way-to-run-docker-in-docker-for-ci](https://getintodevops.com/blog/the-simple-way-to-run-docker-in-docker-for-ci)

[3] Docker Compose Github Releases: [https://github.com/docker/compose/releases](https://github.com/docker/compose/releases)

[4] Advantage of Tini: [https://github.com/krallin/tini/issues/8](https://github.com/krallin/tini/issues/8)

[5] Jenkins behind an NGinX reverse proxy: [https://wiki.jenkins.io/display/JENKINS/Jenkins+behind+an+NGinX+reverse+proxy](https://wiki.jenkins.io/display/JENKINS/Jenkins+behind+an+NGinX+reverse+proxy)
