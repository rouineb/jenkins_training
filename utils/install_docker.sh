#!/usr/bin/env bash

# Purge any old docker version
sudo apt purge docker lxc-docker docker-engine docker.io

# install all needed libraries
sudo apt install curl apt-transport-https ca-certificates software-properties-common

# Import GPG key to verify packages signature
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add

# Add docker repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install engine
sudo apt update && sudo apt install docker-ce
